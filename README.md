## Series Listing App
This app loads the series lists, from the Viaplay API or the content.json file which is located in the root of the project. In this app, I used some technologies which are listed as follow:
```
* JS Framework: React
* CSS Preprocessor: SCSS
* CSS Methodology: BEM
* CSS Framework: Bootstrap
* JS Unit Testing Frameworks: Jest + Enzyme
```

Viaplay API URI: [https://content.viaplay.se/pc-se/serier/samtliga](https://content.viaplay.se/pc-se/serier/samtliga)

**(E.g. jsonObject._embedded[‘viaplay:block’][0]._embedded[‘viaplay:products’])**

## Project usage

### Production
To run the project on the production mode, you just need to upload the build folder of the project on your server and run the index.html file.

### Development
In order to run the project on the development mode, do the following steps:
1. Upload the files on your local/development server and run ```yarn``` to install all the dependencies.
2. After installing all dependencies, run ``` yarn start ``` and wait to run the project on your project via 3000 port (**localhost:3000**).
3. By applying any changes on the files, the server will load the changes by reloading the pages.

### Test
I implemented some simple tests for the components to be sure that everything works as expected. So, to check the test result, run ```yarn test``` on your command line.


## Folders and files structure
I used **create-react-app** for developing this app, so I tried to keep its folder structure. Here are the folder list:
```
- build     => It contains all the production's files
- public    => It has files which are not to be compiled and must be used without any changes on the final app.
- src
|- img  => All the project's images are located in this folder
|- js   => All the JS files and their test files are located here
|- scss => The SCSS files are saved here to generate the file CSS codes
```

## Keyboard Navigation
I tried to implement remote (keyboard) navigation on the app. So I use a JS class which is called **[spatial_navigation.js](https://luke-chang.github.io/js-spatial-navigation/)** to detect the keys to navigate among the series boxes and return to the listing. Currently, you can navigate through the series listing and choosing one of them by pushing the enter. Also on the series detail page, you just need to push escape or backspace button to back to the listing.
Unfortunately, I couldn't test the app by the emulator, but based on my research the below keys are the key the must work with. I tried to add these key codes to my app, but I have no idea whether it works on Smart TVs or not.

```
KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, KEY_ENTER, KEY_RETURN
```

## More description
I tried to add as details as possible comments within the codes. For the Test files, in most cases the title and expectation of the test are clear enough but for more clarification, I added some comments as well.