import React from 'react';
import ReactDOM from 'react-dom';

import App from './js/App'; // main component of the app 
import './scss/index.scss'; // main scss file of the app 

// App component will be added to the div with the "root" ID
ReactDOM.render( <App />, document.getElementById( 'root' ) );
