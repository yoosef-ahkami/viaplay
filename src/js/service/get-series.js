import axios from 'axios'; // library to be used for HTTP ajax requests

// Asynchronous function to return the content from the API or likely errors
export default async () => {

	// const API_URL = 'https://content.viaplay.se/pc-se/serier/samtliga'; // Online API
	const API_URL = './content.json'; // Offline API
	
	let response = { data: null, status: false, error: '' }; // initial response values

	// Try/catch for better handling on the ajax requests
	try {
		const res = await axios.get( API_URL ); 

		response = {
			data: res.data._embedded[ 'viaplay:blocks' ][ 0 ]._embedded[ 'viaplay:products' ],
			status: true
		}
	} catch ( error ) {
		response = { error }
	}

	return response;
}
