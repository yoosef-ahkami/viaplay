import React from 'react'

// this component will display the series boxes on the listing views.
// Set Active series is binding to the component's click event.
const SeriesItem = (props) => {
    const { classNames, info: { content } } = props;
    return (
        <div
            className={`col-sm-6 col-md-3 focusable series-item ${classNames}`}
            onClick={() => props.actionFn(content)}
            onKeyPress={(e) => {
                if (e.key === 'Enter' || e.key === 'KEY_ENTER') {
                    return props.keyPressFn(content)
                }
            }}
        >
            <div className="series-item__inner">
                <div className="series-item__img-box">
                    <img className="series-item__img" src={content.images.landscape.url} alt={content.series.title} />
                </div>
                <div className="series-item__title-box clearfix">
                    <div className="series-item__title">{content.series.title}</div>
                    <div className="series-item__year">{content.production.year}</div>
                </div>
            </div>
        </div>
    )
}

export default SeriesItem
