import React, { Component } from 'react'

// this component will display the series details after clicking on any series's boxes
// remove Active series is binding to the component "series-details__back-btn" click event.

export default class SeriesDetails extends Component {
    constructor(props) {
        super();
    }
    escFunction = e => {
        if (e.keyCode === 27 || e.keyCode === 8 || e.keyCode === 'KEY_RETURN') {
            this.props.actionFn()
        }
    }
    componentDidMount() {
        document.addEventListener("keydown", this.escFunction, false);
    }

    render() {
        const { info } = this.props;
        return (
            <div className="series-details container my-5">
                <div className="series-details__top row">
                    <div className="series-details__img-box col-md-9">
                        <div className="series-details__back-btn"
                            onClick={() => this.props.actionFn()}
                        >
                            <img src="back.svg" alt="Back" />
                        </div>
                        <div className="series-details__img-box">
                            <img className="series-details__img" src={info.images.landscape.url} alt={info.series.title} />
                        </div>
                    </div>
                    <div className="series-details__desc col-md-3">
                        <div className="series-details__desc-top">
                            <h1 className="series-details__title">
                                {info.series.title}
                                <span className="series-details__year">({info.production.year})</span>
                            </h1>
                            <div className="series-details__description">{info.series.synopsis}</div>
                        </div>
                        {
                            info.people &&
                            <div className="series-details__people">
                                {info.people.actors &&
                                    <div className="series-details__people-row">
                                        <span className="series-details__people-row-title">{`Actor${info.people.actors.length > 2 ? 's' : ''}`}: </span>
                                        {info.people.actors.join(', ')}
                                    </div>
                                }
                                {info.people.directors &&
                                    <div className="series-details__people-row">
                                        <span className="series-details__people-row-title">{`Director${info.people.directors.length > 2 ? 's' : ''}`}: </span>
                                        {info.people.directors.join(', ')}
                                    </div>
                                }
                                {info.people.creators &&
                                    <div className="series-details__people-row">
                                        <span className="series-details__people-row-title">{`Creator${info.people.creators.length > 2 ? 's' : ''}`}: </span>
                                        {info.people.creators.join(', ')}
                                    </div>
                                }
                            </div>
                        }
                        {
                            info.imdb &&
                            <div className="series-details__imdb">
                                <span className="series-details__imdb-title">IMDB:</span>
                                <span className="series-details__imdb-rate">
                                    <strong>{info.imdb.rating}</strong>/10
                            </span>
                                <span className="series-details__imdb-votes">
                                    ({info.imdb.votes})
                            </span>
                            </div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}