import React, { Component } from 'react';
import SN from '../lib/spatial_navigation';

// Get series information function
import GetSeries from '../service/get-series';

// The required components
import SeriesItem from './series-item';
import SeriesDetails from './series-details';

export default class Series extends Component {
    constructor(props) {
        super();

        // Initials state of the component
        this.state = {
            series: [],
            error: false,
            activeSeries: null
        }
    }

    componentDidMount() {
        // Getting series information as soon as the component's mounting was finished.
        GetSeries().then(res => {
            if (!res.error) {
                // Setting series information as state of the component
                this.setState({ series: res.data });
            } else {
                // Setting error state to show the required alert
                this.setState({ error: true })
            }
        });

        // Initialize the navigation on component's loading
        window.addEventListener('load', () => {
            this.initializeNavigation()
        });
    }

    componentDidUpdate(prevProps, prevState) {
        // Reinitialize the navigation on back to listing
        if (prevState.activeSeries !== null && this.state.activeSeries === null) {
            this.initializeNavigation()
        }
    }

    // Navigation initialization
    initializeNavigation = () => {
        // Enable Spatial Navigation
        SN.init();
        SN.add({ selector: '.focusable' });
        SN.makeFocusable();
        // Focus the first navigable element.
        SN.focus();
    }

    // This method set the active series to be used in series's detail page
    activeSeriesHandle = (activeSeries) => {
        this.setState({
            activeSeries
        })
    }

    // This method make the active series state of the plugin to null to load the series listing
    removeSeriesHandle = () => {
        this.setState({
            activeSeries: null
        })
    }

    render() {
        const { series, error, activeSeries } = this.state;
        return (
            <div className="main-container">
                {
                    // Show the loading box when the API's status is pending
                    (series.length === 0 && !error) &&
                    <div className="loader-box">
                        <img className="loader-box__img" src="spin.svg" alt="Please wait" />
                    </div>
                }
                {
                    // Show the error alert when the API throws an error
                    (series.length === 0 && error) &&
                    <div className="container">
                        <div className="alert alert-danger">
                            Ooops, Nothing to show!!!
                        </div>
                    </div>
                }
                {
                    // If there is not any errors and active series, it loads the list of the series
                    series.length > 0 && !activeSeries &&
                    <div className="mt-5 mb-5 container">
                        <div className="row">
                            {
                                // include SeriesItem for each of the series
                                // Required functions and the series's information are passed to the component as its props
                                series.map((item, index) => {
                                    const classNames = index === 0 ? 'first' : (index === (series.length - 1)) ? 'last' : '';
                                    return <SeriesItem actionFn={this.activeSeriesHandle} keyPressFn={this.activeSeriesHandle} key={item.system.guid} info={item} classNames={classNames} />
                                })
                            }
                        </div>
                    </div>
                }
                {
                    // If there is at least one series and the activeSeries is set, the series's details page will be loaded
                    // Required functions and the activeSeries's information are passed to the component as its props
                    series.length > 0 && activeSeries &&
                    <SeriesDetails actionFn={this.removeSeriesHandle} info={activeSeries} />
                }
            </div>
        )
    }
}
