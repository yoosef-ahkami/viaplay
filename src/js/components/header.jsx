import React from 'react';

// This component display the header of pages
const Header = (props) => {
	return (
		<header>
			<div className="inner-container container">
				<img className="logo" src="header-logo-large.png" alt="Viaplay Series" />
			</div>
		</header>
	)
}

export default Header
