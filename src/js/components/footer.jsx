import React from 'react';

// This component display the footer of pages
const Footer = (props) => {
	return (
		<footer>
			<div className="inner-container container text-center">
				© 2019 Viasat AB (org.no: 556304-7041). All rights reserved.
			</div>
		</footer>
	)
}

export default Footer
