import seriesData from '../../../public/content.json';

export default {
    get: jest.fn(() => Promise.resolve({ data : seriesData }))
};