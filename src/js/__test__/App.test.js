import React from 'react';
import { configure, shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import App from '../App';

configure( { adapter: new Adapter() } )

describe( 'App Component Tests', () => {
	const AppComp = shallow( <App /> );

	it( 'Component Snapshot', () => {
		expect( shallowToJson( AppComp ) ).toMatchSnapshot();
	} );

	it( 'initial component\'s main tag', () => {
		expect( AppComp.find( '.main-app' ).length ).toEqual( 1 );
	} )
} );
