import React from 'react';
import { configure, shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import { singleSeries } from './series-data';
import SeriesItem from '../components/series-item';

configure( { adapter: new Adapter() } )

describe( 'Series Item Tests', () => {
	let mockCallBack, content, SeriesItemComp;

	beforeEach( () => {
		mockCallBack = jest.fn();
		content = singleSeries.content;
		SeriesItemComp = shallow( <SeriesItem info={singleSeries} actionFn={mockCallBack} /> )
	} );

	it( 'Component Snapshot', () => {
		expect( shallowToJson( SeriesItemComp ) ).toMatchSnapshot();
	} );

	it( 'should show the series\'s year', () => {
		expect( SeriesItemComp.contains( [
			<div className="series-item__title">{content.series.title}</div>,
			<div className="series-item__year">{content.production.year}</div>,
		] ) ).toBe( true )
	} );

	it( 'call the actionFn function', () => {

		// Simulate the click event to test the action function works or not!
		SeriesItemComp.find( '.series-item' ).simulate( 'click' )
		expect( mockCallBack.mock.calls.length ).toEqual( 1 );
		expect( mockCallBack ).toBeCalledWith( singleSeries.content );
	} );
} );
