import React from 'react';
import GetSeries from '../service/get-series';

describe( 'Get Series Tests', () => {
	it( 'should call the GetSeries and get all series information', () => {

		// Check the Axios response by calling the MockAxios and its resolved value
		GetSeries().then( res => {
			if ( !res.error ) {
				// By calling the API, it must have 10 series on its response value
				expect( res.data.length ).toEqual( 10 );
			} else {
				expect( res.error ).toBeTruthy();
			}
		} )
	} );
} );
