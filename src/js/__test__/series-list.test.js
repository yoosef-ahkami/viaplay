import React from 'react';
import { configure, shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import Series from '../components/series-list.jsx';

configure( { adapter: new Adapter() } )

describe( 'Series Details Tests', () => {
	const SeriesComp = shallow( <Series /> );

	it( 'Component Snapshot', () => {
		expect( shallowToJson( SeriesComp ) ).toMatchSnapshot()
	} )

	it( 'initial component\'s state and tags', () => {
		expect( SeriesComp.state().erorr ).not.toBeTruthy();
		expect( SeriesComp.find( '.main-container' ).length ).toEqual( 1 );
	} )

	it( 'should show the error text', () => {

		// Change the status of component to test the alert error's text accordingly
		SeriesComp.setState( { error: true, series: [] } );
		expect( SeriesComp.find( '.alert-danger' ).text() ).toEqual( 'Ooops, Nothing to show!!!' );
	} );
} );
