export const singleSeries = {
	"type": "series",
	"publicPath": "paradise-hotel",
	"content": {
		"images": {
			"boxart": {
				"url": "https://i-viaplay-com.akamaized.net/viaplay-prod/43/764/1567083138-2bcc7f66ba192e31183a4453a22c85ac50ce02b8.jpg?width=199&height=298",
				"template": "https://i-viaplay-com.akamaized.net/viaplay-prod/43/764/1567083138-2bcc7f66ba192e31183a4453a22c85ac50ce02b8.jpg{?width,height}"
			},
			"landscape": {
				"url": "https://i-viaplay-com.akamaized.net/viaplay-prod/43/764/1567083136-9f84431d0f30c5614165a22ffcf6bf0ad388a7e9.jpg?width=960&height=540",
				"template": "https://i-viaplay-com.akamaized.net/viaplay-prod/43/764/1567083136-9f84431d0f30c5614165a22ffcf6bf0ad388a7e9.jpg{?width,height}"
			},
			"hero169": {
				"template": "https://i-viaplay-com.akamaized.net/viaplay-prod/43/764/1567083134-a35ed6f621fafca2ad1a0d9007d34473ad3a9346.jpg{?width,height}"
			},
			"coverart23": {
				"template": "https://i-viaplay-com.akamaized.net/viaplay-prod/43/764/1567083138-2bcc7f66ba192e31183a4453a22c85ac50ce02b8.jpg{?width,height}"
			},
			"coverart169": {
				"template": "https://i-viaplay-com.akamaized.net/viaplay-prod/43/764/1567083136-9f84431d0f30c5614165a22ffcf6bf0ad388a7e9.jpg{?width,height}"
			}
		},
		"originalTitle": "Paradise Hotel Sverige",
		"people": {

		},
		"parentalRating": "15",
		"series": {
			"title": "Paradise Hotel",
			"synopsis": "Ett gäng av glödheta singlar checkar in på lyxhotellet i Mexiko. Förbered er på kärlek, fest, svekfulla pakter och tårar. En halv miljon kronor står på spel och precis allt kan hända i paradiset.",
			"availabilityInfo": "Måndag-onsdag 15.00",
			"seasons": 5,
			"seriesGuid": "series-paradise-hotel-sverige"
		},
		"imdb": {
			"id": "tt0478441",
			"rating": "4.0",
			"votes": "280",
			"url": "http://www.imdb.com/title/tt0478441?ref_ext_viaplay"
		},
		"production": {
			"year": 2013
		}
	},
	"user": {
		"starred": false
	},
	"system": {
		"availability": {
			"start": "2019-07-15T22:00:00.000Z",
			"end": "2022-08-31T22:00:00.000Z",
			"planInfo": {
				"isRental": false,
				"isPurchase": false
			},
			"svod": {
				"start": "2019-07-15T22:00:00.000Z",
				"end": "2022-08-31T22:00:00.000Z",
				"planInfo": {
					"isRental": false,
					"isPurchase": false
				}
			}
		},
		"flags": [
			"hd;sd"
		],
		"guid": "series-paradise-hotel-sverige",
		"isKids": false
	},
	"_links": {
		"self": {
			"title": "Paradise Hotel",
			"href": "https://content.viaplay.se/pc-se/serier/paradise-hotel?partial=true"
		},
		"viaplay:page": {
			"title": "Paradise Hotel",
			"href": "https://content.viaplay.se/pc-se/serier/paradise-hotel"
		},
		"viaplay:templatedPage": {
			"title": "Paradise Hotel",
			"href": "https://content.viaplay.se/{deviceKey}/serier/paradise-hotel"
		},
		"viaplay:genres": [ {
			"title": "Reality",
			"tagId": "108071975",
			"href": "https://content.viaplay.se/pc-se/serier/reality"
		} ],
		"viaplay:peopleSearch": {
			"href": "https://content.viaplay.se/pc-se/search?query=\"{person}\"",
			"templated": true
		}
	},
	"notice": {
		"message": "User must login to view content",
		"code": 1002,
		"_links": {
			"curies": [ {
				"name": "viaplay",
				"href": "http://docs.viaplay.tv/rel/{rel}",
				"templated": true
			} ],
			"viaplay:accountPurchasePackage": {
				"href": "/package?recommended=viaplay",
				"templated": false,
				"redirect": false
			}
		}
	}
}
