import React from 'react';
import { configure, shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import Adapter from 'enzyme-adapter-react-16';
import { singleSeries } from './series-data';
import SeriesDetails from '../components/series-details.jsx';

configure( { adapter: new Adapter() } )

describe( 'Series Details Tests', () => {
	const SeriesDetailsComp = shallow( <SeriesDetails info={singleSeries['content']} /> ),
		{ content } = singleSeries;


	it( 'Component Snapshot', () => {
		expect( shallowToJson( SeriesDetailsComp ) ).toMatchSnapshot();
	} );
	it( 'should show the series\'s title + year', () => {
		expect( SeriesDetailsComp.find( '.series-details__title' ).text() ).toEqual( `${content.series.title}(${content.production.year})` );
	} );

	it( 'should show the series\'s description', () => {
		expect( SeriesDetailsComp.find( '.series-details__description' ).text() ).toEqual( content.series.synopsis );
	} );
} );
