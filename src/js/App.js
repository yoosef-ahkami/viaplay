import React from 'react'

// All the required components imports here
import Header from './components/header';
import Footer from './components/footer';
import Series from './components/series-list';

const App = () => {
	return (
		<div className="main-app">
			<Header />
			<Series />
			<Footer />
		</div>
	)
}

export default App
